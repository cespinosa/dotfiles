# Dotfile's Management

A git bare repository is a elegant way to management our dotfiles.

## Adding our dotfiles to git bare repository

First, we must to create a directory for the new git bare repository, i. e.

```
mkdir $HOME/dotfiles
```

Init the repository

```
git init --bare $HOME/dotfiles
```

We defined a alias to help us (we should add this alias to .bashrc or equivalent file)

```
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME' 
```

Reboot bash, then, we run the next command:

```
config config --local status.showUntrackedFiles no
```

We are ready to add our dotfile to the git bare repository (and we could add a git remote repository). 

Basic usage example:

```
config add /path/to/file
config commit -m "Commit Message"
config remote add origin <repository url>
config push origin master
```

## Install the dotfiles onto a new system

First, we should commit the alias in the new .bashrc file

```
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
```

To avoid weird recursion problems, we add the directory to .gitignore file:

```
echo "dotfiles" >> .gitignore
```

Clone the git bare repository

```
git clone --bare <git-repo-url> $HOME/dotfiles

```

Checkout the actual content from the bare repository to your $HOME:

```
config checkout
```

The previous command might fail:

```
error: The following untracked working tree files would be overwritten by checkout:
    .bashrc
    .gitignore
Please move or remove them before you can switch branches.
Aborting
```

This is because your $HOME folder might already have some stock configuration files which would be overwritten by Git. The solution is simple: back up the files if you care about them, remove them if you don't care. I provide you with a possible rough shortcut to move all the offending files automatically to a backup folder:

```
mkdir -p .config-backup && \
config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{}
```

And re-run the checkout

```
config checkout
```

Finally, we set the flag showUntrackedFiles to No:
```
config config --local status.showUntrackedFiles no
```

And we are ready to add and update our dotfiles.

[Source](https://www.atlassian.com/git/tutorials/dotfiles)
